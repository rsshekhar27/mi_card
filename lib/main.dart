import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        backgroundColor: Colors.deepPurple.shade500,
        body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage(
                  'images/galaxy6.jpg',
                ),
                fit: BoxFit.cover),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                child: DecoratedBox(
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage(
                        'images/galaxy.jpg',
                      ),
                    ),
                  ),
                ),
              ),
              CircleAvatar(
                radius: 50.0,
                backgroundImage: AssetImage(
                  'images/ravi_shekhar.jpg',
                ),
              ),
              Text(
                'Ravi Shekhar',
                style: TextStyle(
                  fontFamily: 'Pacifico',
                  fontSize: 45.00,
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                ),
              ),
              Text(
                'FLUTTER DEVELOPER',
                style: TextStyle(
                    fontFamily: 'Source Sans Pro',
                    fontSize: 20.00,
                    color: Colors.teal.shade100,
                    letterSpacing: 2.5,
                    fontWeight: FontWeight.bold),
              ),
              SizedBox(
                height: 20.0,
                width: 150.0,
                child: Divider(
                  color: Colors.white,
                  thickness: 1.5,
                ),
              ),
              Card(
                color: Colors.white,
                margin: EdgeInsets.symmetric(vertical: 5.0, horizontal: 10.0),
                child: Padding(
                    padding: EdgeInsets.all(5.0),
                    child: ListTile(
                      leading: Icon(
                        Icons.phone,
                        color: Colors.deepPurple,
                      ),
                      title: Text(
                        '+1 123-456-7890',
                        style: TextStyle(
                          color: Colors.deepPurple,
                          fontFamily: 'Source Sans Pro',
                          fontSize: 20.0,
                        ),
                      ),
                    )),
              ),
              Card(
                color: Colors.white,
                margin: EdgeInsets.symmetric(vertical: 5.0, horizontal: 10.0),
                child: Padding(
                    padding: EdgeInsets.all(5.0),
                    child: ListTile(
                      leading: Icon(
                        Icons.email,
                        color: Colors.deepPurple,
                      ),
                      title: Text(
                        'ravi_shekhar@yaahoo.com',
                        style: TextStyle(
                          color: Colors.deepPurple,
                          fontFamily: 'Source Sans Pro',
                          fontSize: 20.0,
                        ),
                      ),
                    )),
              ),
              Card(
                color: Colors.white,
                margin: EdgeInsets.symmetric(vertical: 5.0, horizontal: 10.0),
                child: Padding(
                    padding: EdgeInsets.all(5.0),
                    child: ListTile(
                      leading: FaIcon(
                        FontAwesomeIcons.github,
                        color: Colors.deepPurple,
                      ),
                      title: Text(
                        'ravi_shekhar',
                        style: TextStyle(
                          color: Colors.deepPurple,
                          fontFamily: 'Source Sans Pro',
                          fontSize: 20.0,
                        ),
                      ),
                    )),
              ),
              Card(
                color: Colors.white,
                margin: EdgeInsets.symmetric(vertical: 5.0, horizontal: 10.0),
                child: Padding(
                    padding: EdgeInsets.all(5.0),
                    child: ListTile(
                      leading: FaIcon(
                        FontAwesomeIcons.linkedin,
                        color: Colors.deepPurple,
                      ),
                      title: Text(
                        'ravi_shekhar',
                        style: TextStyle(
                          color: Colors.deepPurple,
                          fontFamily: 'Source Sans Pro',
                          fontSize: 20.0,
                        ),
                      ),
                    )),
              )
            ],
          ),
        ),
      ),
    );
  }
}
